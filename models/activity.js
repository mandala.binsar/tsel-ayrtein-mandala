"use strict";
const { Model } = require("sequelize");
module.exports = (sequelize, DataTypes) => {
  class activity extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      // define association here
      this.belongsToMany(models.student, {
        // studentactivities to studentactivity
        through: "studentactivity",
        foreignKey: "activityId",
        as: "student",
      });
    }
  }
  activity.init(
    {
      name: DataTypes.STRING,
      schedule: DataTypes.DATE,
    },
    {
      sequelize,
      modelName: "activity",
    }
  );
  return activity;
};
