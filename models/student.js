"use strict";
const { Model } = require("sequelize");
module.exports = (sequelize, DataTypes) => {
  class student extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      // define association here
      this.belongsTo(models.school, {
        foreignKey: "schoolId",
        as: "school",
      });
      this.belongsToMany(models.activity, {
        // studentactivities to studentactivity
        through: "studentactivity",
        foreignKey: "studentId",
        as: "activity",
      });
    }
  }
  student.init(
    {
      name: DataTypes.STRING,
      email: DataTypes.STRING,
      schoolId: DataTypes.INTEGER,
      password: DataTypes.STRING,
    },
    {
      sequelize,
      modelName: "student",
    }
  );
  return student;
};
