"use strict";
const { Model } = require("sequelize");
module.exports = (sequelize, DataTypes) => {
  class studentactivity extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      // define association here
    }
  }
  studentactivity.init(
    {
      studentId: DataTypes.INTEGER,
      activityId: DataTypes.INTEGER,
    },
    {
      sequelize,
      modelName: "studentactivity",
    }
  );
  return studentactivity;
};
