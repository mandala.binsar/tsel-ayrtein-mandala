// Using Node.js `require()`
const mongoose = require('mongoose');

const Schema = mongoose.Schema;
//const studentSchema = Schema.ObjectId;

const employeeSchema = new Schema({
  name: { type: String },
  nik: { type: Number },
  position: { type: String },
  isActive: { type: Boolean },
});

module.exports = mongoose.model('employees', employeeSchema);
