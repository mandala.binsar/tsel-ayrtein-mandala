// Import the dependencies for testing
const chai = require('chai');
const chaiHttp = require('chai-http');
const sinon = require('sinon');
const app = require('../../app');
const student = require('../../models').student;
const bcrypt = require('bcrypt');

// Configure chai
chai.use(chaiHttp);
chai.should();

describe('Login Unit Test', () => {
  describe('POST /login', () => {
    beforeEach(function () {
      sinon.restore();
    });

    it('Positive case: login success', (done) => {
      chai
        .request(app)
        .post('/login')
        .send({
          email: process.env.EMAIL,
          password: process.env.PASSWORD,
        })
        .end((err, res) => {
          if (err) done(err);

          res.should.have.status(200);
          res.body.should.to.be.an('object');
          res.body.should.to.have.property('success');
          res.body.success.should.to.equal(true);
          res.body.should.to.have.property('result');
          res.body.should.to.have.property('errorMessage');
          res.body.result.should.to.have.property('token');

          done();
        });
    });

    it('Negative case: cannot connect database', (done) => {
      sinon.stub(student, 'findOne').callsFake(() => {
        return error;
      });

      chai
        .request(app)
        .post('/login')
        .send({
          email: process.env.EMAIL,
          password: process.env.PASSWORD,
        })
        .end((err, res) => {
          if (err) done(err);

          //console.log('Result: ', res.body);
          res.should.have.status(500);
          done();
        });
    });

    it('Negative case: wrong email', (done) => {
      sinon.stub(student, 'findOne').callsFake(() => {
        return null;
      });

      chai
        .request(app)
        .post('/login')
        .send({
          email: process.env.EMAIL,
          password: process.env.PASSWORD,
        })
        .end((err, res) => {
          if (err) done(err);

          //console.log('Result: ', res.body);
          res.should.have.status(400);
          done();
        });
    });
  });

  it('Negative case: wrong password', (done) => {
    sinon.stub(student, 'findOne').callsFake(() => {
      return { password: 'wrong pass' };
    });

    sinon.stub(bcrypt, 'compareSync').callsFake(() => {
      return false;
    });

    chai
      .request(app)
      .post('/login')
      .send({
        email: process.env.EMAIL,
        password: process.env.PASSWORD,
      })
      .end((err, res) => {
        if (err) done(err);

        //console.log('Result: ', res.body);
        res.should.have.status(401);
        done();
      });
  });
});
