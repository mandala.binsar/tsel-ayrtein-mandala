// Import the dependencies for testing
const chai = require('chai');
const chaiHttp = require('chai-http');
const sinon = require('sinon');
const app = require('../../app');
const student = require('../../models').student;
const bcrypt = require('bcrypt');

const token = process.env.TOKEN;

// Configure chai
chai.use(chaiHttp);
chai.should();

describe('Student Unit Test', () => {
  describe('GET students/', () => {
    beforeEach(function () {
      sinon.restore();
    });

    it('Positive case: List all student', (done) => {
      chai
        .request(app)
        .get('/students')
        .set({ Authorization: `Bearer ${token}` })
        .end((err, res) => {
          //console.log('Result: ', res.body);
          res.should.have.status(200);
          res.body.should.to.be.an('object');
          res.body.should.to.have.property('success');
          res.body.success.should.to.equal(true);
          res.body.should.to.have.property('result');
          res.body.should.to.have.property('errorMessage');
          res.body.result.should.to.have.property('student');
          done();
        });
    });

    it('Negative case: cannot connect database', (done) => {
      sinon.stub(student, 'findAll').callsFake(() => {
        return error;
      });

      chai
        .request(app)
        .get('/students')
        .set({ Authorization: `Bearer ${token}` })
        .end((err, res) => {
          if (err) done(err);

          //console.log('Result: ', res.body);
          res.should.have.status(500);
          done();
        });
    });

    it('Positive case: List specific student', (done) => {
      chai
        .request(app)
        .get('/students/1')
        .set({ Authorization: `Bearer ${token}` })
        .end((err, res) => {
          //console.log('Result: ', res.body.result.student.length);
          res.should.have.status(200);
          res.body.should.to.be.an('object');
          res.body.should.to.have.property('success');
          res.body.success.should.to.equal(true);
          res.body.should.to.have.property('result');
          res.body.should.to.have.property('errorMessage');
          res.body.result.should.to.have.property('student');
          res.body.result.student.should.to.be.an('array');
          res.body.result.student.should.to.have.length(1);

          done();
        });
    });

    it('Negative case: cannot connect database', (done) => {
      sinon.stub(student, 'findAll').callsFake(() => {
        return error;
      });

      chai
        .request(app)
        .get('/students/1')
        .set({ Authorization: `Bearer ${token}` })
        .end((err, res) => {
          if (err) done(err);

          //console.log('Result: ', res.body);
          res.should.have.status(500);
          done();
        });
    });
  });
});
