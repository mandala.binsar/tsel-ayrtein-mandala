// Import the dependencies for testing
const chai = require('chai');
const chaiHttp = require('chai-http');
const sinon = require('sinon');
const app = require('../../app');

// Configure chai
chai.use(chaiHttp);
chai.should();

describe('Landing Unit Test', () => {
  describe('GET /', () => {
    it('Pass if status code is 200', (done) => {
      chai
        .request(app)
        .get('/')
        .end((err, res) => {
          res.should.have.status(200);
          done();
        });
    });
  });
});
