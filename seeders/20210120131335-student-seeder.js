"use strict";

module.exports = {
  up: async (queryInterface, Sequelize) => {
    /**
     * Add seed commands here.
     *
     * Example:
     * */
    await queryInterface.bulkInsert(
      "students",
      [
        {
          name: "Ronaldo",
          email: "ronaldo@com",
          password: "ronaldo",
          schoolId: 1,
          createdAt: new Date(),
        },
        {
          name: "Zidane",
          email: "zidane@com",
          password: "zidane",
          schoolId: 1,
          createdAt: new Date(),
        },
        {
          name: "Raul",
          email: "raul@com",
          password: "raul",
          schoolId: 2,
          createdAt: new Date(),
        },
        {
          name: "Budi",
          email: "budi@com",
          password: "budi",
          schoolId: 2,
          createdAt: new Date(),
        },
      ],
      {}
    );
  },

  down: async (queryInterface, Sequelize) => {
    /**
     * Add commands to revert seed here.
     *
     * Example:
     await queryInterface.bulkDelete('students', null, {});
     */
    await queryInterface.bulkDelete("students", null, {});
  },
};
