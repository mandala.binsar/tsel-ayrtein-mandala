"use strict";

module.exports = {
  up: async (queryInterface, Sequelize) => {
    /**
     * Add seed commands here.
     *
     * Example:
     * */
    await queryInterface.bulkInsert(
      "activities",
      [
        {
          name: "Tennis",
          schedule: "2020-01-01",
          createdAt: new Date(),
        },
        {
          name: "Football",
          schedule: "2020-01-01",
          createdAt: new Date(),
        },
        {
          name: "Pulang",
          schedule: "2020-01-01",
          createdAt: new Date(),
        },
      ],

      {}
    );
  },

  down: async (queryInterface, Sequelize) => {
    /**
     * Add commands to revert seed here.
     *
     * Example:
      await queryInterface.bulkDelete('activities', null, {});
     */
    await queryInterface.bulkDelete("activities", null, {});
  },
};
