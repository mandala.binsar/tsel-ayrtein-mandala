"use strict";

module.exports = {
  up: async (queryInterface, Sequelize) => {
    /**
     * Add seed commands here.
     *
     */
    await queryInterface.bulkInsert(
      "studentactivities",
      [
        {
          studentId: 1,
          activityId: 1,
          createdAt: new Date(),
        },
        {
          studentId: 1,
          activityId: 2,
          createdAt: new Date(),
        },
        {
          studentId: 2,
          activityId: 3,
          createdAt: new Date(),
        },
        {
          studentId: 1,
          activityId: 3,
          createdAt: new Date(),
        },
      ],
      {}
    );
  },

  down: async (queryInterface, Sequelize) => {
    /**
     * Add commands to revert seed here.
     */
    await queryInterface.bulkDelete("studentactivities", null, {});
  },
};
