"use strict";

module.exports = {
  up: async (queryInterface, Sequelize) => {
    /**
     * Add seed commands here.
     *
     * Example:
     * */
    await queryInterface.bulkInsert(
      "schools",
      [
        {
          name: "SMP 1 Jakarta",
          address: "Jakarta Pusat",
          createdAt: new Date(),
        },
        {
          name: "SMP 2 Jakarta",
          address: "Jakarta Selatan",
          createdAt: new Date(),
        },
      ],

      {}
    );
  },

  down: async (queryInterface, Sequelize) => {
    /**
     * Add commands to revert seed here.
     *
     * Example:
      //await queryInterface.bulkDelete('schools', null, {});
     */
    await queryInterface.bulkDelete("schools", null, {});
  },
};
