"use strict";

module.exports = {
  up: async (queryInterface, Sequelize) => {
    /**
     * Add altering commands here.
     *
     * Example:
     * await queryInterface.createTable('users', { id: Sequelize.INTEGER });
     */
    await queryInterface.changeColumn("schools", "createdAt", {
      type: Sequelize.DATE,
      allowNull: true,
    });
    await queryInterface.changeColumn("schools", "updatedAt", {
      type: Sequelize.DATE,
      allowNull: true,
    });

    await queryInterface.changeColumn("students", "createdAt", {
      type: Sequelize.DATE,
      allowNull: true,
    });
    await queryInterface.changeColumn("students", "updatedAt", {
      type: Sequelize.DATE,
      allowNull: true,
    });

    await queryInterface.changeColumn("activities", "createdAt", {
      type: Sequelize.DATE,
      allowNull: true,
    });
    await queryInterface.changeColumn("activities", "updatedAt", {
      type: Sequelize.DATE,
      allowNull: true,
    });

    await queryInterface.changeColumn("studentactivities", "createdAt", {
      type: Sequelize.DATE,
      allowNull: true,
    });
    await queryInterface.changeColumn("studentactivities", "updatedAt", {
      type: Sequelize.DATE,
      allowNull: true,
    });
  },

  down: async (queryInterface, Sequelize) => {
    /**
     * Add reverting commands here.
     *
     * Example:
     * await queryInterface.dropTable('users');
     */

    await queryInterface.changeColumn("schools", "createdAt", {
      type: Sequelize.DATE,
      allowNull: false,
    });
    await queryInterface.changeColumn("schools", "updatedAt", {
      type: Sequelize.DATE,
      allowNull: false,
    });

    await queryInterface.changeColumn("students", "createdAt", {
      type: Sequelize.DATE,
      allowNull: false,
    });
    await queryInterface.changeColumn("students", "updatedAt", {
      type: Sequelize.DATE,
      allowNull: false,
    });

    await queryInterface.changeColumn("activities", "createdAt", {
      type: Sequelize.DATE,
      allowNull: false,
    });
    await queryInterface.changeColumn("activities", "updatedAt", {
      type: Sequelize.DATE,
      allowNull: false,
    });

    await queryInterface.changeColumn("studentactivities", "createdAt", {
      type: Sequelize.DATE,
      allowNull: false,
    });
    await queryInterface.changeColumn("studentactivities", "updatedAt", {
      type: Sequelize.DATE,
      allowNull: false,
    });
  },
};
