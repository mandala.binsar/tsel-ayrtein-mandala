var express = require("express");
var router = express.Router();
var passport = require("passport");

const schoolControllers = require("../controllers/schoolControllers");
const schoolControllersObject = new schoolControllers();

router.get(
  //get school list with its student
  "/student",
  passport.authenticate("jwt", { session: false }),
  schoolControllersObject.getSchoolStudent
);
module.exports = router;

//basic CRUD
router.get(
  //get all school
  "/",
  passport.authenticate("jwt", { session: false }),
  schoolControllersObject.getAll
);
router.get(
  //get school
  "/:id",
  passport.authenticate("jwt", { session: false }),
  schoolControllersObject.getByID
);
router.put(
  //edit school
  "/:id",
  passport.authenticate("jwt", { session: false }),
  schoolControllersObject.update
);
router.delete(
  //delete school
  "/:id",
  passport.authenticate("jwt", { session: false }),
  schoolControllersObject.delete
);
router.post(
  //add school
  "/",
  passport.authenticate("jwt", { session: false }),
  schoolControllersObject.add
);
