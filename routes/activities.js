var express = require("express");
var router = express.Router();
var passport = require("passport");

const activityControllers = require("../controllers/activityControllers");
const activityControllersObject = new activityControllers();

router.get(
  //get activities with its student
  "/student",
  passport.authenticate("jwt", { session: false }),
  activityControllersObject.getActivityStudent
);
router.get(
  //get all student
  "/",
  passport.authenticate("jwt", { session: false }),
  activityControllersObject.getAll
);
router.get(
  //get activity by id
  "/:id",
  passport.authenticate("jwt", { session: false }),
  activityControllersObject.getByID
);
router.put(
  //update activity
  "/:id",
  passport.authenticate("jwt", { session: false }),
  activityControllersObject.update
);
router.delete(
  //delete activity
  "/:id",
  passport.authenticate("jwt", { session: false }),
  activityControllersObject.delete
);
router.post(
  //add activity
  "/",
  passport.authenticate("jwt", { session: false }),
  activityControllersObject.add
);

module.exports = router;
