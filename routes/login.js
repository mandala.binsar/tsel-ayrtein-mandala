var express = require('express');
var router = express.Router();

const loginController = require('../controllers/loginControllers');
const loginControllerObject = new loginController();

//login
router.post('/', loginControllerObject.login);
router.post('/refresh', loginControllerObject.refresh);

module.exports = router;
