var express = require("express");
var router = express.Router();
var passport = require("passport");

const studentControllers = require("../controllers/studentControllers");
const studentControllersObject = new studentControllers();

router.get(
  //get students with its activities
  "/activities",
  passport.authenticate("jwt", { session: false }),
  studentControllersObject.getStudentActivity
);

//basic CRUD
router.get(
  //get all student
  "/",
  passport.authenticate("jwt", { session: false }),
  studentControllersObject.getAll
);
router.get(
  //get student
  "/:id",
  passport.authenticate("jwt", { session: false }),
  studentControllersObject.getByID
);
router.put(
  //edit student
  "/:id",
  passport.authenticate("jwt", { session: false }),
  studentControllersObject.update
);
router.delete(
  //delete student
  "/:id",
  passport.authenticate("jwt", { session: false }),
  studentControllersObject.delete
);

//post student or register
router.post("/", studentControllersObject.add);

module.exports = router;
