var express = require('express');
var router = express.Router();

const employeeModel = require('../models/employees');

router.get('/', async function (req, res, next) {
  const employee = await employeeModel.find({});
  res.status(200).send(employee);
});

router.post('/', async function (req, res, next) {
  const employee = {
    name: req.body.name,
    nik: req.body.nik,
    position: req.body.position,
    isActive: req.body.isActive,
  };

  const employeeSave = new employeeModel(employee);

  const result = await employeeSave.save(employee);
  res.status(200).send(result);
});

router.delete('/', async function (req, res, next) {
  const id = req.query.id;
  const employeeDelete = await employeeModel.remove({ _id: id });
  res.status(200).send(employeeDelete);
});

router.put('/', async function (req, res, next) {
  const id = req.query.id;

  const employee = {
    name: req.body.name,
    position: req.body.position,
    isActive: req.body.isActive,
  };

  const employeeDelete = await employeeModel.findByIdAndUpdate(id, employee);
  res.status(200).send(employee);
});

module.exports = router;
