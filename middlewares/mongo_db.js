const mongoose = require('mongoose');

const uri =
  'mongodb+srv://mandala:mandala1234lala@cluster0.m1xgw.mongodb.net/company?retryWrites=true&w=majority&ssl=true';

const options = {
  poolSize: 10,
  reconnectTries: 5,
  reconnectInterval: 500,
};

mongoose.connect(uri, options).then(
  () => {
    console.log('success connect to DB');
  },
  (err) => {
    console.log('fail connect to DB');
  }
);

require('../models/employees');

//const MongoClient = require('mongodb').MongoClient;
// const client = new MongoClient(uri, {
//   useNewUrlParser: true,
//   useUnifiedTopology: true,
// });
// client.connect((err, client) => {
//   if (err) {
//     console.log('Error Occured:');
//     console.log(err);
//     return;
//   }
//   const collection = client.db('school').collection('students');
//   // perform actions on the collection object
//   client.close();
// });
