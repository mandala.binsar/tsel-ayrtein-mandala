const activity = require('../models').activity;
const student = require('../models').student;
const school = require('../models').school;
const refreshToken = require('../models').refreshToken;
const jwt_decode = require('jwt-decode');

class LoginController {
  constructor() {
    this.generateToken = this.generateToken.bind(this);
  }

  async login(req, res) {
    try {
      const email = req.body.email;
      const password = req.body.password;

      const newStudent = await student.findOne({
        where: {
          email: email,
        },
      });

      if (newStudent === null) {
        res.status(400).send({
          success: false,
          result: null,
          errorMessage: 'Email not exist',
        });
        return;
      }

      const bcrypt = require('bcrypt');
      const comparePassword = bcrypt.compareSync(password, newStudent.password);

      if (!comparePassword) {
        res.status(401).send({
          success: false,
          result: null,
          errorMessage: 'Wrong password',
        });
        return;
      }

      let studentObject = newStudent.get();
      delete studentObject.password;

      const JWT = require('jsonwebtoken');

      const token = JWT.sign(
        {
          student: studentObject,
        },
        process.env.SECRETKEY
      );

      const refreshTokenObject = JWT.sign(
        {
          student: studentObject,
        },
        process.env.SECRETKEYREFRESH
      );

      await refreshToken.create({
        token: refreshTokenObject,
      });

      res.status(200).send({
        success: true,
        result: { token: token, refreshToken: refreshTokenObject },
        errorMessage: null,
      });
    } catch (error) {
      console.log(error);
      res.status(500).send({
        success: false,
        result: null,
        errorMessage: 'Internal server error',
      });
    }
  }

  async refresh(req, res) {
    try {
      const token = req.body.token;

      const refreshTokenExist = await refreshToken.findOne({
        where: {
          token: token,
        },
      });

      if (!refreshTokenExist) {
        res.status(401).send({
          success: false,
          result: 'Token Not exist',
          errorMessage: null,
        });
        return;
      }

      await refreshToken.destroy({
        where: {
          id: refreshTokenExist.dataValues.id,
        },
      });

      const JWT = require('jsonwebtoken');
      const decoded = jwt_decode(token);
      const email = decoded.student.email;

      const refreshStudent = await student.findOne({
        where: {
          email: email,
        },
      });

      let studentObject = refreshStudent.get();
      delete studentObject.password;

      const newToken = JWT.sign(
        {
          student: studentObject,
        },
        process.env.SECRETKEY
      );

      const newRefreshToken = JWT.sign(
        {
          student: studentObject,
        },
        process.env.SECRETKEYREFRESH
      );

      await refreshToken.create({
        token: newRefreshToken,
      });

      res.status(200).send({
        success: true,
        result: { token: newToken, refreshToken: newRefreshToken },
        errorMessage: null,
      });
    } catch (error) {
      console.log(error);
      res.status(500).send({
        success: false,
        result: null,
        errorMessage: 'Internal server error',
      });
    }
  }

  async generateToken(student) {
    delete student.password;

    const JWT = require('jsonwebtoken');

    const token = JWT.sign(
      {
        student: student,
      },
      process.env.SECRETKEY
    );

    const refreshToken = JW.sign(
      {
        student: student,
      },
      process.env.SECRETKEYREFRESH
    );

    await refreshToken.create({
      token: refreshToken,
    });

    return { token, refreshToken };
  }
}

module.exports = LoginController;
