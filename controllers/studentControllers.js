const activity = require('../models').activity;
const student = require('../models').student;
const school = require('../models').school;

const bcrypt = require('bcrypt');
const saltRounds = 10;

class StudentController {
  async getAll(req, res) {
    //get students list
    try {
      const output = await student.findAll({
        attributes: { exclude: ['password', 'createdAt', 'updatedAt'] },
      });
      res.status(200).json({
        success: true,
        result: { student: output },
        errorMessage: null,
      });
    } catch (error) {
      //console.log(error);
      res.status(500).json({
        success: false,
        result: null,
        errorMessage: 'Internal server error',
      });
    }
  }

  async getByID(req, res) {
    //get specific student
    try {
      const output = await student.findAll({
        where: {
          id: req.params.id,
        },
        attributes: { exclude: ['password', 'createdAt', 'updatedAt'] },
      });
      res.status(200).json({
        success: true,
        result: { student: output },
        errorMessage: null,
      });
    } catch (error) {
      //console.log('getByID error');
      //console.log(error);
      res.status(500).json({
        success: false,
        result: null,
        errorMessage: 'Internal server error',
      });
    }
  }

  async update(req, res) {
    //update existing student
    try {
      const output = await student.update(
        {
          name: req.body.name,
          email: req.body.email,
          schoold: req.body.schoold,
          password: req.body.password,
          updatedAt: new Date(),
        },
        {
          where: {
            id: req.params.id,
          },
        }
      );

      res.status(200).json({ success: true, result: null, errorMessage: null });
    } catch (error) {
      //console.log(error);
      res.status(500).json({
        success: false,
        result: null,
        errorMessage: 'Internal server error',
      });
    }
  }

  async delete(req, res) {
    //delete existing student
    try {
      await student.destroy({
        where: {
          id: req.params.id,
        },
      });
      res.status(204).json({ success: true, result: null, errorMessage: null });
    } catch (error) {
      //console.log(error);
      res.status(500).json({
        success: false,
        result: null,
        errorMessage: 'Internal server error',
      });
    }
  }

  async add(req, res) {
    // add student || register
    try {
      const hashedPassword = await bcrypt.hash(req.body.password, saltRounds);

      const output = await student.create({
        name: req.body.name,
        email: req.body.email,
        schoolId: req.body.schoolId,
        password: hashedPassword,
        createdAt: new Date(),
      });
      const trimmed = output.get();
      delete trimmed.password;
      delete trimmed.createdAt;
      delete trimmed.updatedAt;

      res.status(200).json({
        success: true,
        result: { student: trimmed },
        errorMessage: null,
      });
    } catch (error) {
      //console.log(error);
      res.status(500).json({
        success: false,
        result: null,
        errorMessage: 'Internal server error',
      });
    }
  }

  async getStudentActivity(req, res) {
    //get all student with its activities
    try {
      const output = await student.findAll({
        attributes: { exclude: ['password', 'createdAt', 'updatedAt'] },
        include: [
          'activity',
          {
            model: activity,
            as: 'activity',
            attributes: ['id', 'name', 'schedule'],
            through: {
              attributes: [],
            },
          },
        ],
      });
      res.status(200).json({
        success: true,
        result: { student: output },
        errorMessage: null,
      });
    } catch (error) {
      //console.log(error);
      res.status(500).json({
        success: false,
        result: null,
        errorMessage: 'Internal server error',
      });
    }
  }
}

module.exports = StudentController;
