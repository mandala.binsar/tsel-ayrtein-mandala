const activity = require('../models').activity;
const student = require('../models').student;
const school = require('../models').school;

class SchoolController {
  async getAll(req, res) {
    //get schools list
    try {
      const output = await school.findAll({
        attributes: { exclude: ['createdAt', 'updatedAt'] },
      });
      res.status(200).json({
        success: true,
        result: { school: output },
        errorMessage: null,
      });
    } catch (error) {
      console.log(error);
      res.status(500).json({
        success: false,
        result: null,
        errorMessage: 'Internal server error',
      });
    }
  }

  async getByID(req, res) {
    //get specific school
    try {
      const output = await school.findAll({
        where: {
          id: req.params.id,
        },
        attributes: { exclude: ['createdAt', 'updatedAt'] },
      });
      res.status(200).json({
        success: true,
        result: { school: output },
        errorMessage: null,
      });
    } catch (error) {
      console.log('getByID error');
      console.log(error);
      res.status(500).json({
        success: false,
        result: null,
        errorMessage: 'Internal server error',
      });
    }
  }

  async update(req, res) {
    //update existing school
    try {
      const output = await school.update(
        {
          name: req.body.name,
          address: req.body.address,
          updatedAt: new Date(),
        },
        {
          where: {
            id: req.params.id,
          },
        }
      );
      res.status(200).json({
        success: true,
        result: null,
        errorMessage: null,
      });
    } catch (error) {
      console.log(error);
      res.status(500).json({
        success: false,
        result: null,
        errorMessage: 'Internal server error',
      });
    }
  }

  async delete(req, res) {
    //delete existing school
    try {
      await school.destroy({
        where: {
          id: req.params.id,
        },
      });
      res.status(204).json({
        success: true,
        result: null,
        errorMessage: null,
      });
    } catch (error) {
      console.log(error);
      res.status(500).json({
        success: false,
        result: null,
        errorMessage: 'Internal server error',
      });
    }
  }

  async add(req, res) {
    //add school
    try {
      const output = await school.create({
        name: req.body.name,
        address: req.body.address,
        createdAt: new Date(),
      });

      const trimmed = output.get();
      delete trimmed.createdAt;
      delete trimmed.updatedAt;

      res.status(200).json({
        success: true,
        result: { school: trimmed },
        errorMessage: null,
      });
    } catch (error) {
      console.log(error);
      res.status(500).json({
        success: false,
        result: null,
        errorMessage: 'Internal server error',
      });
    }
  }

  async getSchoolStudent(req, res) {
    //get all school with its students
    try {
      console.log('invoked');
      const output = await school.findAll({
        attributes: { exclude: ['createdAt', 'updatedAt'] },
        include: [
          {
            model: student,
            as: 'student',
            attributes: {
              exclude: ['password', 'createdAt', 'updatedAt', 'schoolId'],
            },
          },
        ],
      });
      res.status(200).json({
        success: true,
        result: { school: output },
        errorMessage: null,
      });
    } catch (error) {
      console.log(error);
      console.log('getSchoolsStudents error');

      res.status(500).json({
        success: false,
        result: null,
        errorMessage: 'Internal server error',
      });
    }
  }
}

module.exports = SchoolController;
