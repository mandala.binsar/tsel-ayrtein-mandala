const activity = require('../models').activity;
const student = require('../models').student;
const school = require('../models').school;

class ActivityController {
  //get all activity
  async getAll(req, res) {
    try {
      const output = await activity.findAll({
        attributes: { exclude: ['createdAt', 'updatedAt'] },
      });
      res.status(200).send({
        success: true,
        result: { activity: output },
        errorMessage: null,
      });
    } catch (error) {
      console.log(error);
      res.status(500).send({
        success: false,
        result: null,
        errorMessage: 'Internal server error',
      });
    }
  }

  async getByID(req, res) {
    //get activity by ID
    try {
      const output = await activity.findAll({
        where: {
          id: req.params.id,
        },
        attributes: { exclude: ['createdAt', 'updatedAt'] },
      });
      res.status(200).send({
        success: true,
        result: { activity: output },
        errorMessage: null,
      });
    } catch (error) {
      console.log('getByID error');
      console.log(error);
      res.status(500).send({
        success: false,
        result: null,
        errorMessage: 'Internal server error',
      });
    }
  }

  async update(req, res) {
    //update acitivy
    try {
      const output = await activity.update(
        {
          name: req.body.name,
          schedule: req.body.schedule,
          updatedAt: new Date(),
        },
        {
          where: {
            id: req.params.id,
          },
        }
      );
      res.status(200).send({
        success: true,
        result: null,
        errorMessage: null,
      });
    } catch (error) {
      console.log(error);
      res.status(500).send({
        success: false,
        result: null,
        errorMessage: 'Internal server error',
      });
    }
  }

  async delete(req, res) {
    //delete activiyu
    try {
      await activity.destroy({
        where: {
          id: req.params.id,
        },
      });
      res.status(204).send({
        success: true,
        result: null,
        errorMessage: null,
      });
    } catch (error) {
      console.log(error);
      res.status(500).send({
        success: false,
        result: null,
        errorMessage: 'Internal server error',
      });
    }
  }

  async add(req, res) {
    //add acitivy
    try {
      const output = await activity.create({
        name: req.body.name,
        schedule: req.body.schedule,
        createdAt: new Date(),
      });

      const trimmed = output.get();
      delete trimmed.createdAt;
      delete trimmed.updatedAt;

      res.status(200).json({
        success: true,
        result: { activity: trimmed },
        errorMessage: null,
      });
    } catch (error) {
      console.log(error);
      res.status(500).send({
        success: false,
        result: null,
        errorMessage: 'Internal server error',
      });
    }
  }

  async getActivityStudent(req, res) {
    //get acitities with student
    try {
      const output = await activity.findAll({
        attributes: { exclude: ['createdAt', 'updatedAt'] },
        include: [
          'student',
          {
            model: student,
            as: 'student',
            attributes: ['id', 'name', 'email', 'schoolId'],
            through: {
              attributes: [],
            },
          },
        ],
      });
      res.status(200).json({
        success: true,
        result: { activity: output },
        errorMessage: null,
      });
    } catch (error) {
      console.log(error);
      res.status(500).json({
        success: false,
        result: null,
        errorMessage: 'Internal server error',
      });
    }
  }
}

module.exports = ActivityController;
